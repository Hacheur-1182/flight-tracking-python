const mymap = L.map('mapid').setView([7.0332361, 12.7273712], 6);
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(mymap);


mapMarkers1 = [];
mapMarkers2 = [];
mapMarkers3 = [];
mapMarkers4 = [];
mapMarkers5 = [];


const dataAirport = [
    {
        "lat": 3.726944,
        "long": 11.553056,
        "city": "Yaoundé"
    },
    {
        "lat": 4.0011667,
        "long": 9.708333,
        "city": "Douala"
    },
    {
        "lat": 8.978333,
        "long": 38.799444,
        "city": "Addis Abbe."
    },
    {
        "lat": 9.2575,
        "long": 12.430278,
        "city": "Yola"
    },
    {
        "lat": 9.335833,
        "long": 13.37,
        "city": "Garoua"
    },
    {
        "lat": 37.362728,
        "long": 121.929167,
        "city": "Jos"
    },
    {
        "lat": 4.95,
        "long": 8.325,
        "city": "Calabar"
    },
    {
        "lat": 4.398611,
        "long": 18.518889,
        "city": "Bangui"
    },
    {
        "lat": 12.133611,
        "long": 15.033889,
        "city": "N'Djamena"
    }
]


const airportIcon = L.icon({
    iconUrl: `static/airport.png`,
    iconSize: [30, 30], // size of the icon
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
})


dataAirport.forEach((airportInfo) => {
    const airportMarker = L.marker([airportInfo.lat, airportInfo.long], {icon: airportIcon});
    airportMarker.bindPopup(`<b>${airportInfo.city}</b> Airport`).openPopup();
    airportMarker.addTo(mymap);
})


let is_display = false;
let line1;
let line2;
let line3;
let line4;

const iconImg = (imgNumber) => {
    return L.icon({
        iconUrl: `static/plane${imgNumber}.png`,
        iconSize: [30, 30], // size of the icon
        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
    })

}


function myFunction(mapMarkers) {
    return mapMarkers.map((position) => {
        let lat = position.getLatLng().lat;
        let long = position.getLatLng().lng;
        return [lat, long];
    });
}

function calculate_distance(mark1, mark2) {
    let rad_lat1;
    let rad_lat2;
    let rad_theta;
    let dist;
    if (mark1.length > 0 && mark2.length > 0) {
        let lat1 = parseFloat(mark1[mark1.length - 1].getLatLng().lat);
        let long1 = parseFloat(mark1[mark1.length - 1].getLatLng().lng);
        let lat2 = parseFloat(mark2[mark2.length - 1].getLatLng().lat);
        let long2 = parseFloat(mark2[mark2.length - 1].getLatLng().lng);

        if (lat1 === lat2 && long1 === long2) {
            return 0
        }
        rad_lat1 = Math.PI * lat1 / 180
        rad_lat2 = Math.PI * lat2 / 180
        console.log(Math.PI);

        rad_theta = long1 - long2
        dist = (Math.sin(rad_lat1) * Math.sin(rad_lat2)) + (Math.cos(rad_lat1) * Math.cos(rad_lat2) * Math.cos(rad_theta))

        if (dist > 1) {
            return 1
        }
        dist = Math.acos(dist)
        dist = dist * 180 / Math.PI
        dist = dist * 60 * 1.1515
        dist = dist * 1.609344
        return dist;
    }
}


const source = new EventSource('/topic/flightdata');

source.addEventListener('message', function (e) {

    let i;

    // console.log('Message');
    let obj = JSON.parse(e.data);
    console.log(obj);

    if (is_display === true) {

        var marks1 = myFunction(mapMarkers1)
        var marks2 = myFunction(mapMarkers2)
        var marks3 = myFunction(mapMarkers3)
        var marks4 = myFunction(mapMarkers4)
        const line1 = L.polyline(marks1, {color: 'blue'});
        const line2 = L.polyline(marks2, {color: 'red'});
        const line3 = L.polyline(marks3, {color: 'green'});
        const line4 = L.polyline(marks4, {color: 'yellow'});

        line1.addTo(mymap);
        line2.addTo(mymap);
        line3.addTo(mymap);
        line4.addTo(mymap);
    } else {
        // Il faut supprimer la ligne

        console.log("her")
        if (typeof line1 !== 'undefined') {
            line1.remove()
        }
    }
    ;

    mymap.on('dblclick', function (ev) {
        // console.log(ev) // ev is an event object (MouseEvent in this case)
        console.log("before", is_display)
        is_display = !is_display
        console.log("after", is_display)
        // x
    });


    if (obj.flight === 'Dash- Q400') {
        for (i = 0; i < mapMarkers1.length; i++) {
            mymap.removeLayer(mapMarkers1[i]);
        }

        const marker1 = L.marker([obj.latitude, obj.longitude], {icon: iconImg(3)});
        marker1.bindPopup(`Compagny: <b>${obj.compagny}</b><br>Airplane Type: ${obj.Airplane_type}<br>Departure: ${obj.Departure_time}<br>Arrival: ${obj.Arrival_time}<br>Speed: ${obj.speed}<br>Altitude: ${obj.Flight_altitude}.`).openPopup();

        marker1.addTo(mymap);

        if (mapMarkers1.length >= 0) {
            // c'est quand on va cliquer sur le vol que le tracé doit se faire
            // const line1 = L.polyline([mapMarkers1[0].getLatLng(),marker1.getLatLng()], {color:'blue'});
            // line1.bindPopup("line1").openPopup();
            // line1.addTo(mymap);
            mymap.on('dblclick', function (ev) {
                // console.log(ev) // ev is an event object (MouseEvent in this case)
                let markers = mapMarkers1.map((position) => {
                    let lat = position.getLatLng().lat;
                    let long = position.getLatLng().lng;
                    return [lat, long];
                })

                const line1 = L.polyline(markers, {color: 'blue'});
                line1.addTo(mymap);
            });
            marker1.setLatLng(L.latLng(obj.latitude, obj.longitude))

        }
        mapMarkers1.push(marker1);

    }
    ;

    if (obj.flight === 'Boeing 767-300 ER') {
        for (i = 0; i < mapMarkers2.length; i++) {
            mymap.removeLayer(mapMarkers2[i]);
        }
        // marker2 = L.marker([obj.latitude, obj.longitude], {icon: iconImg(3)}).addTo(mymap);
        const marker2 = L.marker([obj.latitude, obj.longitude], {icon: iconImg(3)});
        marker2.bindPopup(`Compagny: <b>${obj.compagny}</b><br>Airplane Type: ${obj.Airplane_type}<br>Departure: ${obj.Departure_time}<br>Arrival: ${obj.Arrival_time}<br>Speed: ${obj.speed}<br>Altitude: ${obj.Flight_altitude}.`).openPopup();

        marker2.addTo(mymap);

        if (mapMarkers2.length > 0) {
            marker2.setLatLng(L.latLng(obj.latitude, obj.longitude))
            mymap.on('dblclick', function (ev) {
                // console.log(ev) // ev is an event object (MouseEvent in this case)
                let markers = mapMarkers2.map((position) => {
                    let lat = position.getLatLng().lat;
                    let long = position.getLatLng().lng;
                    return [lat, long];
                })

                const line1 = L.polyline(markers, {color: 'red'});
                line1.addTo(mymap);
            });
            // line2 = L.polyline([mapMarkers2[mapMarkers2.length-1].getLatLng(),marker2.getLatLng()], {color:'red'}).addTo(mymap);
        }
        mapMarkers2.push(marker2);
    }

    if (obj.flight === 'Embraer ERJ-145') {
        for (i = 0; i < mapMarkers3.length; i++) {
            mymap.removeLayer(mapMarkers3[i]);
        }
        // marker3 = L.marker([obj.latitude, obj.longitude], {icon: iconImg(3)}).addTo(mymap);
        const marker3 = L.marker([obj.latitude, obj.longitude], {icon: iconImg(4)});
        marker3.bindPopup(`Compagny: <b>${obj.compagny}</b><br>Airplane Type: ${obj.Airplane_type}<br>Departure: ${obj.Departure_time}<br>Arrival: ${obj.Arrival_time}<br>Speed: ${obj.speed}<br>Altitude: ${obj.Flight_altitude}.`).openPopup();

        marker3.addTo(mymap);

        if (mapMarkers3.length > 0) {

            marker3.setLatLng(L.latLng(obj.latitude, obj.longitude))
        }
        mapMarkers3.push(marker3);
    }

    if (obj.flight === 'Boeing 737') {
        for (i = 0; i < mapMarkers4.length; i++) {
            mymap.removeLayer(mapMarkers4[i]);
        }

        const marker4 = L.marker([obj.latitude, obj.longitude], {icon: iconImg(2)});
        marker4.bindPopup(`Compagny: <b>${obj.compagny}</b><br>Airplane Type: ${obj.Airplane_type}<br>Departure: ${obj.Departure_time}<br>Arrival: ${obj.Arrival_time}<br>Speed: ${obj.speed}<br>Altitude: ${obj.Flight_altitude}.`).openPopup();

        marker4.addTo(mymap);


        if (mapMarkers4.length > 0) {

            marker4.setLatLng(L.latLng(obj.latitude, obj.longitude))
            distance1 = calculate_distance(mapMarkers4, mapMarkers2)
            distance2 = calculate_distance(mapMarkers4, mapMarkers3)
            distance3 = calculate_distance(mapMarkers1, mapMarkers4)

            console.log(distance2)
            if (distance2 < 3000) {
                alert("attention le vol Yaoundé - Bangui est proche celui Addis Abeba - Yaoundé ")

            }
        }
        mapMarkers4.push(marker4);

    }

    if (obj.airline === 'Lufthansa') {
        for (i = 0; i < mapMarkers5.length; i++) {
            mymap.removeLayer(mapMarkers5[i]);
        }
        const marker5 = L.marker([obj.latitude, obj.longitude], {icon: iconImg(3)});
        marker5.bindPopup(`Compagny: <b>${obj.compagny}</b><br>Airplane Type: ${obj.Airplane_type}<br>Departure: ${obj.Departure_time}<br>Arrival: ${obj.Arrival_time}<br>Speed: ${obj.speed}<br>Altitude: ${obj.Flight_altitude}.`).openPopup();

        marker5.addTo(mymap);
        if (mapMarkers5.length > 0) {
            line5 = L.polyline([mapMarkers5[mapMarkers5.length - 1].getLatLng(), marker5.getLatLng()], {color: 'yellow'}).addTo(mymap);
        }
        mapMarkers5.push(marker5);
    }
}, false);



