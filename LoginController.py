from flask_security.forms import LoginForm
from flask_security.utils import get_message
from flask import redirect
from wtforms import StringField
from flask import request
from models import User
from werkzeug.security import generate_password_hash, check_password_hash
from flask_security import hash_password


class MyLogin(LoginForm):
    # super().username = request.form.get("id")

    username = StringField("username")
    password = StringField("password")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def validate(self, **kwargs):
        user = User.query.filter(User.username == self.username.data).first()

        if user is None:
            
            self.form_errors.append("Check credentials")
          
            #self.form_errors.append(msg)
            return False
        if not check_password_hash(pwhash=user.password, password=self.password.data):
            msg = get_message("INVALID_PASSWORD")[0]
            self.form_errors.append(msg)
            return False

        self.user = user

        # if not super().validate(**kwargs):
        #             return False

        return True
