# Setup 
    Install requirements with command 
        pip install -r requirements.txt
# Flight

    I registered three itinerary  :
        - From Yde to Gra
        - from N'djamena to Yde
        - from Garoua to Dla
        - addis abeba to Yde
        - Bangui to Yde
        - Yola to Yde
        
# Authentication 
    I created two accounts 
      - superadmin: 
            username = arthur
            password = arthur
      - contrôleur
            username = mballa
            password = mballa
# Activate Flight

    localhost:5001/flight/1/activate
    
# Logout
    To log out : route /logout
