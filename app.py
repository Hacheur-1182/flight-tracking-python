from flask import Flask, render_template, Response, render_template_string, request, url_for
from pykafka import KafkaClient
from default import DevelopmentConfig
from flask_security import Security, current_user, login_required, auth_required, \
    SQLAlchemySessionUserDatastore, roles_required
from database import db_session, init_db
from models import User, Role
from LoginController import MyLogin
from datetime import datetime, timedelta
from pykafka.common import OffsetType
import flight1
import flight2
import flight3
import flight4


# from werkzeug.security import generate_password_hash, check_password_hash


def get_kafka_client():
    return KafkaClient(hosts='127.0.0.1:9092')


app = Flask(__name__)
app.config.from_object(DevelopmentConfig())

user_datastore = SQLAlchemySessionUserDatastore(db_session, User, Role)
app.security = Security(app, user_datastore, login_form=MyLogin)


@app.context_processor
def login_context():
    return {
        'login_user_form': MyLogin(),
    }


@app.route("/flight/<int:id>/activate")
def activation(id):
    if id == 1:

        flight1.generateCurrentLocation()
    elif id == 2:
        flight2.generateCurrentLocation()
    elif id == 3:
        flight3.generateCurrentLocation()
    elif id == 4:
        flight4.generateCurrentLocation()
    return ""


@app.route("/")
@auth_required()
# @roles_required('contr')
def home():
    return render_template('index.html')


@app.route("/aviation")
@auth_required()
# @roles_required('contr')
def aviation():
    return render_template('aviation.html')


# with app.app_context():

#     if not app.security.datastore.find_user(email="mballa@me.com"):
#         app.security.datastore.create_user(email="mballa@me.com", username="mballa",
#         password=generate_password_hash("mballa"), roles=["contrôleur"])
#     db_session.commit()

#     if not app.security.datastore.find_user(email="arthur@me.com"):
#         app.security.datastore.create_user(email="arthur@me.com", username="arthur",
#         password=generate_password_hash("arthur"), roles=["contrôleur", "superadmin"])
#     db_session.commit()

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@app.route('/topic/<topicname>')
def get_messages(topicname):
    client = get_kafka_client()

    def events():
        for i in client.topics[topicname].get_simple_consumer(
                auto_offset_reset=OffsetType.LATEST,
                reset_offset_on_start=True
        ):
            yield 'data:{0}\n\n'.format(i.value.decode())

    return Response(events(), mimetype="text/event-stream")


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=5001)
