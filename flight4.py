from pykafka import KafkaClient
import json
from datetime import datetime
import uuid
import time


def generateCurrentLocation():
    print("Ethiopian Airline Aircraft Boeing 737 is taking off...")

    input_file = open('./data/addis abeba - Yde.json')
    json_array = json.load(input_file)
    coordinates = json_array['features'][0]['geometry']['coordinates']

    # KAFKA PRODUCER
    client = KafkaClient(hosts="localhost:9092")
    topic = client.topics['flightdata']
    producer = topic.get_sync_producer()


    # GENERATE UUID
    def generate_uuid():
        return uuid.uuid4()


    # CONSTRUCT MESSAGE AND SEND IT TO KAFKA
    data = {}
    data['airline'] = 'Ethopia Airline'
    data['flight'] = 'Boeing 737'
    data['arrival'] = 'Addis Abeba'
    data['departure'] = 'Yaoundé, Nsimalen'

    data['company'] = 'Overland Airways'
    data['Airplane_type'] = 'Boeing B-29'
    data['Departure_time'] = '8h'
    data['Arrival_time'] = '11h45'
    data['speed'] = '436 KTS'
    data['Flight_altitude'] = '290 FL'

    data['line'] = coordinates


    # i = 0
    # while i < len(coordinates):
    for i in range(len(coordinates)):
        data['key'] = data['airline'] + '_' + str(generate_uuid())
        data['timestamp'] = str(datetime.utcnow())
        data['latitude'] = coordinates[i][1]
        data['longitude'] = coordinates[i][0]
        message = json.dumps(data)
        producer.produce(message.encode('ascii'))
        time.sleep(5)

    print("Ethiopian Airline Aircraft Boeing 737 has touch down...")

#generateCurrentLocation(coordinates)