from pykafka import KafkaClient
import json
from datetime import datetime
import uuid
import time

#READ COORDINATES FROM GEOJSON



def generateCurrentLocation():
    print("Camair-Co Aircraf Boeing 767-300 ER is taking off...")

    input_file = open('./data/Yola-Yde.json')
    json_array = json.load(input_file)
    coordinates = json_array['features'][0]['geometry']['coordinates']

    # KAFKA PRODUCER
    client = KafkaClient(hosts="localhost:9092")
    topic = client.topics['flightdata']
    producer = topic.get_sync_producer()


    # GENERATE UUID
    def generate_uuid():
        return uuid.uuid4()


    # CONSTRUCT MESSAGE AND SEND IT TO KAFKA
    data = {}
    data['airline'] = 'Camair-co'
    data['flight'] = 'Boeing 767-300 ER'
    data['departure'] = 'Yola'
    data['arrival'] = 'Yaoundé, Nsimalen'

    data['company'] = 'Camair-co'
    data['Airplane_type'] = 'DH8Delta'
    data['Departure_time'] = '12h30'
    data['Arrival_time'] = '14h'
    data['speed'] = '324 KTS'
    data['Flight_altitude'] = '250 FL'

    data['line'] = coordinates


    # i = 0
    # while i < len(coordinates):
    for i in range(len(coordinates)):
        data['key'] = data['airline'] + '_' + str(generate_uuid())
        data['timestamp'] = str(datetime.utcnow())
        data['latitude'] = coordinates[i][1]
        data['longitude'] = coordinates[i][0]
        message = json.dumps(data)
        producer.produce(message.encode('ascii'))
        time.sleep(5)

    print("Camair-Co Aircraf Boeing 767-300 ER has touch down...")


#generateCurrentLocation(coordinates)