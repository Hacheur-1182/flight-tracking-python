from database import Base
from flask_security import UserMixin, RoleMixin
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.mutable import MutableList
from sqlalchemy import Boolean, DateTime, Column, Integer, \
                    String, ForeignKey, Text, Float

class RolesUsers(Base):
    __tablename__ = 'roles_users'
    id = Column(Integer(), primary_key=True)
    user_id = Column('user_id', Integer(), ForeignKey('user.id'))
    role_id = Column('role_id', Integer(), ForeignKey('role.id'))

class Role(Base, RoleMixin):
    __tablename__ = 'role'
    id = Column(Integer(), primary_key=True)
    name = Column(String(80), unique=True)
    description = Column(String(255), nullable=True)
    #permissions = Column(MutableList.as_mutable(Asalist()), nullable=True)

class User(Base, UserMixin):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    email = Column(String(255), unique=True)
    username = Column(String(255), unique=True, nullable=True)
    password = Column(String(255), nullable=False)
    last_login_at = Column(DateTime())
    current_login_at = Column(DateTime())
    #last_login_ip = Column(String(100))
    #current_login_ip = Column(String(100))
    login_count = Column(Integer, nullable=True)
    active = Column(Boolean(), nullable=True)
    fs_uniquifier = Column(String(64), unique=True, nullable=True)
    confirmed_at = Column(DateTime(), nullable=True)
    roles = relationship('Role', secondary='roles_users',
                         backref=backref('users', lazy='dynamic'))
    def __repr__(self):
        return f'<User {self.username!r}>'
    

# class Airport(Base):
#     __tablename__ = 'airport'
#     id = Column(Integer, primary_key=True, autoincrement=True)
#     name = Column(Text, nullable=True)
#     acronym = Column(String(4), nullable=True)
#     coordinate = Column(Text, nullable=True)
#     created_at = Column(DateTime, nullable=True)
#     country = Column(String(255), nullable=True)
#     itinerary = relationship("Itinerary", back_populates="airport_1", lazy=True, cascade="all, delete-orphan")
#     departure = relationship("Flight", back_populates="departure_airport", lazy="dynamic", cascade="all, "
#                                                                                                       "delete-orphan")
#     arrival = relationship("Flight", back_populates="arrival_airport", lazy='dynamic', cascade="all, delete-orphan")


class Itinerary(Base):
    __tablename__ = 'itinerary'
    id = Column(Integer, primary_key=True, autoincrement=True)
    description = Column(Text, nullable=True, default=None)
    # airport_1 = Column(Integer, ForeignKey("airport.id"), nullable=True)
    # airport_2 = Column(Integer, ForeignKey("airport.id"), nullable=True)
    airport_1 = Column(Text, nullable=False)
    airport_2 = Column(Text, nullable=False)
    path = Column(Text, nullable=False)
    created_at = Column(DateTime, nullable=False)

    def __repr__(self):
        return f"Itinerary(id={self.id}, path={self.path})"


class Flight(Base):
    __tablename__ = 'flight'
    id = Column(Integer, primary_key=True, autoincrement=True)
    # departure_airport = Column(Integer, ForeignKey("airport.id"), nullable=True)
    # arrival_airport = Column(Integer, ForeignKey("airport.id"), nullable=True)   
    departure_airport = Column(Text, nullable=False)
    arrival_airport = Column(Text, nullable=False)
    distance = Column(Float, nullable=False)
    departure_time = Column(DateTime, nullable=False)
    arrival_time = Column(DateTime, nullable=False)
    created_at = Column(DateTime, nullable=False)
    itinerary = Column(Integer, ForeignKey("itinerary.id"), nullable=True)
    aircraft = Column(Text, nullable=True)

