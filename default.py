import os
from dotenv import load_dotenv, find_dotenv
from flask_security import uia_username_mapper

load_dotenv(find_dotenv())


class Config(object):
    TESTING = False
    SECRET_KEY = os.environ.get("KEY")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    JWT_SECRET_KEY = "HS256"
    SECURITY_PASSWORD_SALT=os.environ.get("SALT")
    SECURITY_EMAIL_VALIDATOR_ARGS = {"check_deliverability": False}
    SECURITY_USER_IDENTITY_ATTRIBUTES = [
        {"username": {"mapper": uia_username_mapper, "case_insensitive": False}}
    ]



class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")


class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = "sqlite://flight-track.db"
    TESTING = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
