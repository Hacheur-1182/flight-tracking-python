from database import db_session
from werkzeug.security import generate_password_hash
from datetime import datetime, timedelta
from models import User, Role, Itinerary, Flight

'''
    Add User
'''
# u = User(
#     username="eg",
#     email="eg@gmail.com",
#     password=generate_password_hash("eg"),
#     fs_uniquifier="xxalkflkn@lknglkng"
# )

'''
    Add Role
'''
# r = Role(
#     name="contrôleur",
#     description="Il peut contrôler le trafic aerien"
# )
# r_2= Role(
#     name="superadmin",
#     description="Il peut ajouter user"
# )

'''
    add role to user
'''

#user = User.query.filter(User.username == 'arthur').first()
# role = Role.query.filter(Role.name == 'contrôleur').first()
# u.roles.append(role) 
# 
'''
    add itinerary
''' 
# it_1 = Itinerary(
#     description="itineraire 1 Garoua - Douala",
#     airport_1="Garoua",
#     airport_2="Douala",
#     path="data/Gra-Douala.geojson",
#     created_at=datetime.now()
# )

# it_2 = Itinerary(
#     description="itineraire 1 N'djamena - Yaoundé",
#     airport_1="N'djamena",
#     airport_2="Yaoundé",
#     path="data/nd'jam-Yde-1.geojson",
#     created_at=datetime.now()
# )

# it_3 = Itinerary(
#     description="itineraire 1 Yaoundé - Garoua",
#     airport_1="Garoua",
#     airport_2="Yaoundé",
#     path="data/yde-gra-1.geojson",
#     created_at=datetime.now()
# )
# db_session.add_all([it_1, it_2, it_3])

'''
    Add Flight 
'''
# departure = datetime.now() + timedelta(hours=10)
# flight = Flight(
#   departure_airport="Yaoundé",
#   arrival_airport="Garoua",
#   distance=645,
#   departure_time=datetime.now() + timedelta(hours=10),
#   arrival_time=departure + timedelta(hours=1, minutes=21),
#   created_at=datetime.now(),
#   itinerary=3,
#   aircraft="Boeing 767-300 ER [01]"
# )

db_session.add(u)

db_session.commit()