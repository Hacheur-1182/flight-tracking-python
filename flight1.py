from pykafka import KafkaClient
import json
from datetime import datetime
import uuid
import time



def generateCurrentLocation():

    # READ COORDINATES FROM GEOJSON
    print("Camair-Co Aircraf Dash- Q400 is taking off...")

    input_file = open('./data/yde-gra.json')
    json_array = json.load(input_file)
    coordinates = json_array['features'][0]['geometry']['coordinates']

    # KAFKA PRODUCER
    client = KafkaClient(hosts="localhost:9092")
    topic = client.topics['flightdata']
    producer = topic.get_sync_producer()


    # GENERATE UUID
    def generate_uuid():
        return uuid.uuid4()


    # CONSTRUCT MESSAGE AND SEND IT TO KAFKA
    data = {}
    data['airline'] = 'Camair-co'
    data['flight'] = 'Dash- Q400'
    data['departure'] = 'Garoua'
    data['arrival'] = 'Yaoundé, Nsimalen'

    data['company'] = 'Ethiopian Airlines'
    data['Airplane_type'] = 'BOEING 789'
    data['Departure_time'] = '14h45'
    data['Arrival_time'] = '19h30'
    data['speed'] = '487 KTS'
    data['Flight_altitude'] = '390 FL'

    data['line'] = coordinates

    # i = 0
    # while i < len(coordinates):
    for i in range(len(coordinates)):
        data['key'] = data['airline'] + '_' + str(generate_uuid())
        data['timestamp'] = str(datetime.utcnow())
        data['latitude'] = coordinates[i][1]
        data['longitude'] = coordinates[i][0]
        message = json.dumps(data)
        producer.produce(message.encode('ascii'))
        time.sleep(5)

        # #if flight reaches last coordinate, start from beginning
    
        # else:
        #     i += 1
    # exit()
    print("Camair-Co Aircraf Dash-Q400 has touched down...")


#generateCurrentLocation(coordinates)
