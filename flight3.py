from pykafka import KafkaClient
import json
from datetime import datetime
import uuid
import time



def generateCurrentLocation():
    print("Camair-Co Aircraf Embraer ERJ-145 is taking off...")

    input_file = open('./data/Bangui-Yde.json')
    json_array = json.load(input_file)
    coordinates = json_array['features'][0]['geometry']['coordinates']

    # KAFKA PRODUCER
    client = KafkaClient(hosts="localhost:9092")
    topic = client.topics['flightdata']
    producer = topic.get_sync_producer()


    # GENERATE UUID
    def generate_uuid():
        return uuid.uuid4()


    # CONSTRUCT MESSAGE AND SEND IT TO KAFKA
    data = {}
    data['airline'] = 'Camair-co'
    data['flight'] = 'Embraer ERJ-145'
    data['departure'] = 'Bangui'
    data['arrival'] = 'Yaoundé, Nsimalen'

    data['company'] = 'Camair-co'
    data['Airplane_type'] = 'Boeing 737'
    data['Departure_time'] = '9h30'
    data['Arrival_time'] = '10h25'
    data['speed'] = '455 KTS'
    data['Flight_altitude'] = '370 FL'

    data['line'] = coordinates

    # i = 0
    # while i < len(coordinates):
    for i in range(len(coordinates)):
        data['key'] = data['airline'] + '_' + str(generate_uuid())
        data['timestamp'] = str(datetime.utcnow())
        data['latitude'] = coordinates[i][1]
        data['longitude'] = coordinates[i][0]
        message = json.dumps(data)
        producer.produce(message.encode('ascii'))
        time.sleep(5)

    print("Camair-Co Aircraf Embraer ERJ-145 has touch down...")
#generateCurrentLocation(coordinates)